package com.galvanize;

import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;
import java.util.Random;

public class Main {

    static int guessCount = 0;
    static int gameCount = 0;
    static ArrayList<Integer> bestGame =  new ArrayList<>();

    public static void main(String[] args) {

        gameIntro();

    }


    public static void gameIntro(){

        System.out.println("This program allows you to play a guessing game. ");
        System.out.println("I will think of a number between 1 and 100 and will allow you to guess until you get it.");
        System.out.println("For each guess, I will tell you whether the right answer is higher or lower than your guess.");
        System.out.println(" * in my best Joshua voice * ");
        System.out.println("Shall we play a game?");
        System.out.println("Please enter Yes or No");


        Scanner input = new Scanner(System.in);  // Create a Scanner object

        String answer = input.next();  // Read user input

        if (answer.toLowerCase().equals("yes")){
            gameStart();
        }else{
            gameEnd();
        }

    }

//==== New Game ====\\
    public static void gameStart() {

        boolean win = false;
        int count = 0;

        int randomNumber = (int) (Math.random() * 100) + 1;
        System.out.println("Please enter a number between 1 and 100.");
        System.out.print("Your guess? ");
        Scanner input = new Scanner(System.in);
        int userNumber = input.nextInt();

        while (win == false){

            count++;
            guessCount++;

            if (userNumber < randomNumber) {
                System.out.println("It's higher.");
                System.out.print("Your guess? ");
                userNumber = input.nextInt();
            } else if (userNumber > randomNumber) {
                System.out.println("It's lower.");
                System.out.print("Your guess? ");
                userNumber = input.nextInt();
            } else {
                win = true;
                System.out.println("You are correct!");
                System.out.println("You got it right in " + count + " guesses!");
            }
        }
        gameCount++;
        bestGame.add(count);

        System.out.print("Shall we play again? ");
        String answer = input.next();  // Read user input

        if (answer.equalsIgnoreCase("y")){
            gameStart();
        }else{
            gameEnd();
        }
    }


//==== End Game ====\\

        public static void gameEnd(){

            int gameAvg = guessCount / gameCount;
            int min = bestGame.get(0);

            for (int i = 0; i <bestGame.size() ; i++) {
                if (bestGame.get(i) < min){
                    min = bestGame.get(i);
                }
            }

            System.out.println();
            System.out.println("Overall results: ");

            System.out.println("Total games = " + gameCount);
            System.out.println("Total guesses = "+ guessCount);
            System.out.println("Average guess per game = " + gameAvg);
            System.out.println("Best game = " + min);

            System.out.println();
            System.out.println("Goodbye.");
    }


}